   console.log("Hello World");

   let firstName = "John";
   console.log("First Name: " + firstName);

   let lastName = "Smith";
   console.log("Last Name: " + lastName);

   let currentAge = 30;
   console.log("Age: " + currentAge);

   console.log("Hobbies:");

   let myHobbies = ["Biking", "Mountain Climbing","Swimming"];  
   console.log(myHobbies);

   console.log("Work Address:");

   let workAddress = {
      houseNumber: '32',
      street: 'Washington',
      city: 'Lincoln',
      state: 'Nebraska'
   }
   console.log(workAddress);

   let fullName = "Steve Rogers";
   let yourAge = 40;
   console.log("My full name: is: " + fullName);
   console.log("My current age is " + yourAge);

   console.log("My Friends Are:");

   let myFriends = ["Tony", "Bruce","Thor", "Natasha", "Clint","Nick"];
   console.log(myFriends);  

   console.log("My Full Profile:");

   let fullProfile = {
   username: 'captain_america',
   fullName: 'Steve Rogers',
   age: 40,
   isActive: false
   }
   console.log(fullProfile);


   let friendName = "Bucky Barnes";
   console.log("My bestfriend is: " + friendName);
   let friendPlace = "Arctic Ocean";
   console.log("I was found frozen in: " + friendPlace);


   

 